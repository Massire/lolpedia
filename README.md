
![Logo](https://logo-marque.com/wp-content/uploads/2020/11/League-of-Legends-Logo-2009-2019.png)

# LOL pedia

Un projet vous permettant de récupérer et voir les informations des champions selon votre rôle



## Installation

Install my-project with npm

```bash
  git clone https://gitlab.com/Massire/lolpedia.git
  cd lolpedia
```

### Installation backend
```bash
  cd /backend
  npm install
```

### Installation frontend
```bash
  cd ../frontend
  npm install
```
    
## Run Locally

### Local version
Start the backend server

```bash
  npm run dev
```

Start the frontend app
```
  npm run dev
```

### Docker version
Move to racine version

```bash
  docker compose up
```


## Authors

- [@Vladimir](https://www.gitlab.com/vovacode)
- [@Yasser](https://www.gitlab.com/ysrakechi)
- [@Rohat](https://www.gitlab.com/Rohat954)
- [@Massiré](https://www.gitlab.com/Massire)

