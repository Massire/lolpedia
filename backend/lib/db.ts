import {DataTypes, Sequelize} from "sequelize";

const sequelize = new Sequelize({
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '123456',
    database: 'lolpedia',
    dialect: 'mysql'
});


export const ChampionModel = sequelize.define('champions', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: DataTypes.STRING,
    formated: DataTypes.STRING,
    blurb: DataTypes.STRING,
    tags: DataTypes.JSON,
    keys: DataTypes.STRING
},
    {timestamps: false}
);

export default sequelize;