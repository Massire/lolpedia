import express from 'express'
import cors from 'cors'
import { SDK } from 'casdoor-nodejs-sdk'
import * as dotenv from 'dotenv'
import cookieParser from 'cookie-parser'
import sequelize, { ChampionModel } from "./lib/db";
import { Op } from "sequelize";

dotenv.config({
    path: __dirname + '/.env'
})

const app = express()
const sdk = new SDK({
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    orgName: process.env.ORGANIZATION_NAME,
    appName: process.env.APP_NAME,
    certificate: process.env.CERTIFICATE,
    endpoint: process.env.CASDOOR_URL
})

const PORT= parseInt(process.env.SERVER_PORT as string)

app.use(express.json())
app.use(cors({
    origin: true,
    credentials: true,
}))
app.use(cookieParser())

app.post('/api/get-token', async (req, res) => {
    const { code } = req.query

    const token = await sdk.getAuthToken(code.toString())
    const user = await sdk.parseJwtToken(token.access_token)
    res.cookie('token', token.access_token, { maxAge: 60 * 60 * 24 * 1000 })
    return res.json({ user, token, success: true }).status(200)
})

app.get('/api/champions', async (req, res) => {
    const tags = req.query.tags as string[]

    const query = tags.map(tag => { return { [Op.substring]: tag.substring(0, 1).toUpperCase()+tag.substring(1) } })

    const result = await ChampionModel.findAll({
        where: {
            tags: {
                [Op.or]:
                    query
            }
        }
    })

    res.send({ champions: result }).status(200)
})

app.get('/api/test', async(req, res) => {
    return res.json('[SERVER]: Service is running up').status(200)
})

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`)
    console.log(process.env)
})