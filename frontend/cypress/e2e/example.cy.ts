// https://on.cypress.io/api

describe('Casdoor Login Test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8000/login');
  });

  it('should successfully log in', () => {
    cy.get('input[id="input"]').type('admin');
    cy.get('input[id="normal_login_password"]').type('123');
    cy.get('button[type="submit"]').click();

    // (correct returned URL http://localhost:8000/ -> Home page)
    cy.url().should('eq', 'http://localhost:8000/');
  });
});