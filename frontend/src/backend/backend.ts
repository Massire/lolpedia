import { casdoorConfig } from '@/main'
import axios from 'axios'

async function getAccount() {
  return await axios.get(`${casdoorConfig.serverUrl}/api/get-account`, {
    withCredentials: true
  })
}

async function logOut() {
  const response = await axios.get(`${casdoorConfig.serverUrl}/api/logout`, {
    method: 'POST',
    withCredentials: true
  })
  localStorage.removeItem('token')
  return response
}

async function getChampions() {
  const {
    data: {
      data: { groups }
    }
  } = await getAccount()

  const tag = (group: string) => group.split('/')[1]

  const tags = groups.map(tag)

  const {
    data: { champions }
  } = await axios.get(`http://localhost:5001/api/champions`, {
    params: { tags },
    paramsSerializer: {
      indexes: true
    }
  })

  return champions
}

export default {
  getAccount,
  getChampions,
  logOut
}
