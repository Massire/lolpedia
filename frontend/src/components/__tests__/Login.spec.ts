import { mount } from '@vue/test-utils';
import LoginView from '@/views/LoginView.vue';
import {describe, it, expect} from "vitest";
import Casdoor from "casdoor-vue-sdk";
import {casdoorConfig} from "../../main";
import HomeView from "../../views/HomeView.vue";
import App from "../../App.vue";
import AuthCallback from "../../views/AuthCallback.vue";

const config = {
    serverUrl: "http://localhost:8000",
    clientId: "8b5bcdf7d23c070247ff",
    organizationName: "built-in",
    appName: "app-built-in",
    redirectPath: "/callback"
}

describe('LoginView', () => {

    it('renders "Login" button', async () => {
        const wrapper = mount(LoginView,{global:{plugins: [Casdoor]}, props:{}});

        expect(wrapper.find('button:contains("Login")').exists()).toBe(true);

    });
});

describe('App', () => {
    it('renders App', async () => {
        const wrapper = mount(App,{global:{plugins: [Casdoor]}, props:{}});

        expect(wrapper.find('div').exists()).toBe(true);

    });
});

describe('AuthCallback', () => {
    it('renders Callback page message', () => {
        const wrapper = mount(AuthCallback, {global:{plugins: [Casdoor]}, props:{}})
        expect(wrapper.find('h1').exists()).toBe(true)
    })
})

describe('HomeView', () => {
    it('renders properly the main', () => {
        const wrapper = mount(HomeView)
        expect(wrapper.find('main').exists()).toBe(true)
    })
})

describe('config casdoor', () =>{
    it('config is good', () => {
        expect(config.serverUrl).toEqual(casdoorConfig.serverUrl)
        expect(config.clientId).toEqual(casdoorConfig.clientId)
        expect(config.organizationName).toEqual(casdoorConfig.organizationName)
        expect(config.appName).toEqual(casdoorConfig.appName)
        expect(config.redirectPath).toEqual(casdoorConfig.redirectPath)
    })
})