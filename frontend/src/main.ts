import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import PrimeVue from 'primevue/config';
import Casdoor from 'casdoor-vue-sdk'
import 'primevue/resources/themes/arya-blue/theme.css';


import App from '@/App.vue'
import router from './router'
import Button from "primevue/button";
import DataTable from "primevue/datatable";
import Column from "primevue/column";

const app = createApp(App)
export const casdoorConfig = {
    serverUrl: "http://localhost:8000",
    clientId: "0caa66264495d187cc5c",
    organizationName: "built-in",
    appName: "app-built-in",
    redirectPath: "/callback"
}
app.use(createPinia())
app.use(router)
app.use(PrimeVue);
app.use(Casdoor, casdoorConfig)

// eslint-disable-next-line vue/multi-word-component-names,vue/no-reserved-component-names
app.component('Button', Button);
app.component('DataTable', DataTable);
app.component('Column', Column);

app.mount('#app')
