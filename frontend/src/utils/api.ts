import axios from "axios"
const jwt = () => localStorage.getItem("token") ?? null

export const isLogged = () => jwt() !== null


const api = axios.create({
    transformRequest: [
        (data, headers) => {
            headers.authentication = jwt()
            return JSON.stringify(data)
        },
    ],
})

export default api